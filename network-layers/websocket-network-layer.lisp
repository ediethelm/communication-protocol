;;;; Copyright (c) Eric Diethelm 2021 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)

(defconstant max-message-buffer-size 2048)

(defclass WebSocket-Network-Layer (Network-layer)
  ((type :initform :web-socket
	 :type symbol
	 :reader layer-type
	 :allocation :class)
   (port :type (integer 0 65535)
	 :reader port
	 :initform 8081)
   (interface :type t
	      :reader interface
	      :initform nil)
   (server :type t
	   :reader server
	   :initform nil)
   (subscriptions :type list
		  :reader subscriptions
		  :initform nil)
   (resources :initform nil
	      :reader resources
	      :type list)
   (thread :type t
	   :reader thread
	   :initform nil)))

(defmethod make-frame-generator ((event Message) (layer WebSocket-Network-Layer))
  (let* ((json (trivial-json-codec:serialize-json event))
	 (frames (list (substitute #\_ #\- json :end (length json)))))
    (trivial-utilities:blambda ()
      (prog1
	  (car frames)
	(setf frames (cdr frames))))))

(defmethod send ((message Message) (layer WebSocket-Network-Layer))
  (let* ((device-id (source-id message))
	 (message-id (message-id message))
	 (frame-generator (make-frame-generator message layer))
	 (subs (trivial-object-lock:with-object-lock-held (layer :property 'subscriptions)
		 (remove-if-not
		  (lambda (x)
		    (eq (getf x :message-id) message-id))
		  (second (assoc device-id (subscriptions layer)))))))

    (let ((data (funcall frame-generator)))
      (iterate:iterate
	(iterate:for subscription in subs)
	(trivial-utilities:awhen (car (hunchensocket:clients (getf subscription :resource)))
	  (hunchensocket:send-text-message
	   it
	   data)))

      ;; Call (callback-fn message) informing whether some subscriber was notified
      )))

(defmethod load-configuration ((layer WebSocket-Network-Layer) config)
  (setf (slot-value layer 'port) (getf config :port))

  (let ((interface (getf config :interface))
	(interfaces (ip-interfaces:get-ip-interfaces-by-flags '(:iff-running :iff-up))))
    (if interface
	(trivial-utilities:aif (find-if #'(lambda (x) (string-equal interface (ip-interfaces:ip-interface-name x))) interfaces)
			       (setf (slot-value layer 'interface) it)
			       (progn
				 (log:error "The given interface '~a' does not exist." interface)
				 (error "The given interface '~a' does not exist." interface)))
	(let ((net (remove (car (ip-interfaces:get-ip-interfaces-by-flags '(:iff-running :iff-up :iff-loopback)))
			   (ip-interfaces:get-ip-interfaces-by-flags '(:iff-running :iff-up)) :test #'equalp)))
	  (when (> (length net) 1)
	    (log:warn "More than one interface detected. Using first one.")
	    (warn "More than one interface detected. Using first one."))

	  (when (eq (length net) 0)
	    (log:error "No interface detected.")
	    (error "No interface detected."))

	  (setf (slot-value layer 'interface) (car net))))))

(defmethod start ((layer WebSocket-Network-Layer))
  (setf (slot-value layer 'server)
	(make-instance 'hunchensocket:websocket-acceptor :port (port layer)))

  (setf (slot-value layer 'resources)
	(list (make-instance 'resource :layer layer :name "/ws-network")))
  
  (pushnew #'(lambda (request) (find-resource request layer))
	   hunchensocket:*websocket-dispatch-table*)

  (hunchentoot:start (server layer))

  (return-from start t))

(defmethod stop ((layer WebSocket-Network-Layer))
  (map nil #'hunchensocket::close-connection
       (mapcan #'hunchensocket:clients (resources layer)))

  (hunchentoot:stop (server layer))
  (setf (slot-value layer 'server) nil)
  (setf (slot-value layer 'interface) nil)
  (setf (slot-value layer 'subscriptions) nil)

  (return-from stop t))

;;------------------------------------------------

(defclass resource (hunchensocket:websocket-resource)
  ((name :initarg :name :initform (error "Name this resource!") :reader name) ;;-> should include device id
   (layer :initarg :layer :initform (error "Define the layer of this resource!") :reader layer))
  (:default-initargs :client-class 'user))

(defclass user (hunchensocket:websocket-client)
  ((name :initarg :user-agent :reader name :initform (error "Name this user!"))))

(defun find-resource (request layer)
  (find (hunchentoot:script-name request) (resources layer) :test #'(lambda (x y) (declare (type simple-string x y)) (string= x y)) :key #'name))


;;---------- Subscription handling ----------

(defun add-user-subscription (user resource device-id message-id)
  (declare (type user user)
	   (type resource resource)
	   (type (unsigned-byte 9) device-id)
	   (type (unsigned-byte 10) message-id))
  (let* ((layer (layer resource))
	 (subscriptions (subscriptions layer))
	 (device-subscription (assoc device-id subscriptions))
	 (entry (list :user user :message-id message-id :resource resource)))
    (if device-subscription
	(if (find entry (second device-subscription) :test #'equal)
	    (log:warn "Repeated subscription entry add: Device-ID ~a, Message-ID ~a, User ~a, Resource ~a"
		      device-id message-id user resource)
	    (setf (second device-subscription)
		  (nconc (second device-subscription) (list entry))))
	(progn
	  (push (list device-id (list entry))
		(slot-value layer 'subscriptions))
	  (add-device device-id layer (communication-interface layer))))))


(defun delete-user-subscription (user resource device-id message-id)
  (declare (type user user)
	   (type resource resource)
	   (type (unsigned-byte 9) device-id)
	   (type (unsigned-byte 10) message-id))
  (let* ((layer (layer resource))
	 (subscriptions (subscriptions layer))
	 (device-subscription (assoc device-id subscriptions))
	 (entry (list :user user :message-id message-id :resource resource)))
    (if (and device-subscription
	     (find entry (second device-subscription) :test #'equal))
	(setf (second device-subscription) (delete entry (second device-subscription) :test #'equal))
	(log:warn "Missing subscription entry delete: Device-ID ~a, Message-ID ~a, User ~a, Resource ~a"
		  device-id message-id user resource))

    (when (and device-subscription
	       (not (second device-subscription)))
      (remove-device device-id layer (communication-interface layer))
      (setf (slot-value layer 'subscriptions)
	    (delete device-id subscriptions :key #'car)))))


(defun delete-all-subscriptions-from-user (user resource)
  (declare (type user user)
	   (type resource resource))
  (let* ((layer (layer resource))
	 (subscriptions (subscriptions layer))
	 (removed-devices nil))
    (iterate:iterate
      (iterate:for subscription in subscriptions)
      (setf (second subscription)
	    (delete user (second subscription) :key #'(lambda (x) (getf x :user))))
      (unless (second subscription)
	(remove-known-device (first subscription) layer (network-router (communication-interface layer)))
	(push (first subscription) removed-devices)))
    (setf (slot-value layer 'subscriptions)
	  (delete-if #'(lambda (x) (member x removed-devices)) subscriptions :key #'car))))

;;-------------------------------------------


(defmethod hunchensocket:client-connected ((resource resource) user)
  (declare (ignore resource user)))

(defmethod hunchensocket:client-disconnected ((resource resource) user)
  ;; Remove all subscriptions of that user
  (delete-all-subscriptions-from-user user resource))

(defmethod hunchensocket:text-message-received ((resource resource) (user user) (json-str string))
  (let ((json (trivial-json-codec:deserialize-raw
	       (substitute #\- #\_ json-str))))
    ;;(log:info "Message received: ~a from '~a' via ~a" json (name user) resource)

  (if (assoc :subscription-action json)
      (cond ((string-equal (cadr (assoc :subscription-action json)) "subscribe")
	     (add-user-subscription user resource (cadr (assoc :source-id json)) (cadr (assoc :message-id json))))
	    ((string-equal (cadr (assoc :subscription-action json)) "unsubscribe")
	     (delete-user-subscription user resource (cadr (assoc :source-id json)) (cadr (assoc :message-id json))))
	    (t (log:error "Invalid action ('~a') received from '~a'." (cadr (assoc :subscription-action json)) (name user))))

      (let ((message (make-message-from-frame json)))
	;; WebSocket is a Point-to-Point protocol, so we have to inform all subscribers also
	;; This is not handled by the  Network-Router, as it does not call the originating layer again.
	(send message (layer resource))

	(funcall (on-receive-fn (layer resource))
		 message
		 (layer resource))))))

