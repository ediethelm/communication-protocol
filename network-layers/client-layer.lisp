;;;; Copyright (c) Eric Diethelm 2021 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)

(defclass Client-Layer (Network-Layer)
  ((type :initform :client
	 :type symbol
	 :reader layer-type
	 :allocation :class)
   (on-event-callback :reader on-event-callback
		      :type function
		      :initarg :on-event-callback)
   (on-service-request-callback :reader on-service-request-callback
				:type function
				:initarg :on-service-request-callback)
   (service-response-timeout :initarg :service-response-timeout
			     :type (integer 0 *)
			     :reader service-response-timeout
			     :initform 1
			     :documentation "Time to wait for a device response after a service request (in seconds).")
   (service-response-monitoring-thread-id :initarg :service-response-monitoring-thread-id
					  :type (or null (integer 0 *))
					  :reader service-response-monitoring-thread-id)
   (service-response-handlers :initarg :service-response-handlers
			      :reader service-response-handlers
			      :type t
			      :initform (make-service-response-handler-map))))

(defmethod start ((layer Client-Layer))
  (start-response-monitor layer))

(defmethod stop ((layer Client-Layer))
  (stop-response-monitor layer))

(defmethod send ((message Message) (layer Client-Layer))
  (cond ((typep message 'Event)
	 ;; @TODO Add a concept of own source id to ignore RTR requests to other devices
	 ;;(if (or (not (rtr message))
	 ;;	   (eq (source-id message) (source-id (communication-interface layer))))
	 (funcall (on-event-callback layer) message layer))
	((typep message 'Service-Request)
	 (funcall (on-service-request-callback layer) message))
	((typep message 'Service-Response)
	 (let ((handler (pop-service-response-handler (cons (source-id message) (message-id message)) layer)))
	   (if handler
	       (let ((handler handler))
		 (declare (type function handler))
		 (trivial-utilities:awhen (funcall handler message)
		   (forward-message it layer)))
	       (log:warn "No registered service response handler for service ~a from device ~a." (message-id message) (source-id message)))))
	(t (log:warn "Unknown message class '~a'." (type-of message)))))

(defun forward-message (message layer)
  (when (typep message 'Service-Request)
    (register-service-response-handler (cons (destination-id message) (1+ (message-id message))) message layer))
  (funcall (on-receive-fn layer) message layer))

(defun make-service-response-handler-map ()
  #+sbcl (make-hash-table :test #'equal)
  #+ccl (make-hash-table :test #'equal :shared t)
  #+ecl (make-hash-table :synchronized t))

(defun register-service-response-handler (dev&msg-id service layer)
  #+sbcl
  (sb-ext:with-locked-hash-table ((service-response-handlers layer))
    (setf (gethash dev&msg-id (service-response-handlers layer)) (list (get-universal-time) service 0)))
  #+(or ecl ccl)
  (setf (gethash dev&msg-id (service-response-handlers layer)) (list (get-universal-time) service 0)))

(defun unregister-service-response-handler (dev&msg-id layer)
  #+sbcl
  (sb-ext:with-locked-hash-table ((service-response-handlers layer))
    (remhash dev&msg-id (service-response-handlers layer)))
  #+(or ecl ccl)
  (remhash dev&msg-id (service-response-handlers layer)))

(defun start-response-monitor (layer)
  (declare (type Client-Layer layer))

  (flet ((inner-loop-body (srh timeout)
	   (loop
	     with now of-type (unsigned-byte 62) = (get-universal-time)
	     for dev&msg-id of-type cons being the hash-keys in srh
	       using (hash-value value)
	     when (> now (+  timeout (the (unsigned-byte 62) (car value))))
	       do
		  (progn
		    (remhash dev&msg-id srh)
		    (funcall (callback-fn (second value)) (second value) :service-request-failed :reason :response-timeout)))))
    (setf (slot-value layer 'service-response-monitoring-thread-id)
	  (trivial-monitored-thread:make-monitored-thread (:name "Service-Response-Monitoring-Thread" :sleep-duration 0.025 :max-restarts 5)
	    #+sbcl
	    (sb-ext:with-locked-hash-table ((service-response-handlers layer))
	      (inner-loop-body (service-response-handlers layer) (service-response-timeout layer)))
	    #+(or ecl ccl)
	    (inner-loop-body service-response-handlers (service-response-timeout layer))))))

(defun stop-response-monitor (layer)
  (declare (type Client-Layer layer))
  (when (service-response-monitoring-thread-id layer)
    (trivial-monitored-thread:stop-thread (service-response-monitoring-thread-id layer))
    (trivial-monitored-thread:join-thread (service-response-monitoring-thread-id layer))
    (setf (slot-value layer 'service-response-monitoring-thread-id) nil)))

(defun get-service-response-handler (dev&msg-id layer)
  (declare (type Client-Layer layer))
  (trivial-utilities:aif
    #+sbcl
    (sb-ext:with-locked-hash-table ((service-response-handlers layer))
      (second (gethash dev&msg-id (service-response-handlers layer))))
    #+(or ecl ccl)
    (second (gethash dev&msg-id (service-response-handlers layer)))
    (response-handler-generator-fn it)
    nil))

(defun pop-service-response-handler (dev&msg-id layer)
  (declare (type Client-Layer layer))
  (trivial-utilities:aif
    #+sbcl
    (sb-ext:with-locked-hash-table ((service-response-handlers layer))
      (prog1
	  (second (gethash dev&msg-id (service-response-handlers layer)))
	(remhash dev&msg-id (service-response-handlers layer))))
    #+(or ecl ccl)
    (prog1
	(second (gethash dev&msg-id (service-response-handlers layer)))
      (remhash dev&msg-id (service-response-handlers layer)))
    (response-handler-generator-fn it)
    nil))

