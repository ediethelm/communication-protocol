;;;; Copyright (c) Eric Diethelm 2018 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)

(defconstant max-message-buffer-size 2048)
(defconstant +SEND-LENGTH-LIMIT+ 512 "Maximal length of data to be sent in one message.")

(defclass UDP-Network-Layer (Network-Layer)
  ((type :initform :udp
	 :type symbol
	 :reader layer-type
	 :allocation :class)
   (port :type (integer 0 65535)
	 :reader port
	 :initform 4242)
   (interface :type t
	      :reader interface
	      :initform nil)
   (broadcast-address :type t
		      :reader broadcast-address
		      :initform nil)
   (socket :type t
	   :reader socket
	   :initform nil)
   (send-length-limit :reader send-length-limit
		      :type (integer 0 *)
		      :initarg :send-length-limit
		      :initform 512)
   (message-buffer :initform (make-array max-message-buffer-size :element-type '(unsigned-byte 8))
		   :reader buffer)
   (udp-receive-thread :type t
		       :reader udp-receive-thread
		       :initform nil)
   (shutdown-requested :type boolean
		       :initform nil
		       :reader shutdown-requested)
   (redundant-messages :type boolean
		       :initform nil
		       :reader redundant-messages-p)
   (rm-resent-count :type (unsigned-byte 1)
		       :initform 0
		       :reader redundant-messages-resend-count)
   (rm-resend-interval :type (unsigned-byte 1)
		       :initform 0
		       :reader redundant-messages-resend-interval)))


(defmethod load-configuration ((layer UDP-Network-Layer) config)
  (setf (slot-value layer 'port) (or (getf config :port)
				     (slot-value layer 'port)))
  (setf (slot-value layer 'redundant-messages) (getf config :redundant-messages))
  (setf (slot-value layer 'rm-resent-count) (or (getf config :rm-resent-count)
						(slot-value layer 'rm-resent-count)))
  (setf (slot-value layer 'rm-resend-interval) (or (getf config :rm-resend-interval)
						   (slot-value layer 'rm-resend-interval)))

  (let ((interface (getf config :interface))
	(interfaces (ip-interfaces:get-ip-interfaces-by-flags '(:iff-running :iff-up))))
      (if interface
	  (trivial-utilities:aif (find-if #'(lambda (x) (string-equal interface (ip-interfaces:ip-interface-name x))) interfaces)
				 (setf (slot-value layer 'interface) it)
				 (progn
				   (log:error "The given interface '~a' does not exist." interface)
				   (error "The given interface '~a' does not exist." interface)))
	  (let ((net (remove (car (ip-interfaces:get-ip-interfaces-by-flags '(:iff-running :iff-up :iff-loopback)))
			     (ip-interfaces:get-ip-interfaces-by-flags '(:iff-running :iff-up)) :test #'equalp)))
	    (when (> (length net) 1)
	      (log:warn "More than one interface detected. Using first one.")
	      (warn "More than one interface detected. Using first one."))

	    (when (eq (length net) 0)
	      (log:error "No interface detected.")
	      (error "No interface detected."))

	    (setf (slot-value layer 'interface) (car net)))))

  (setf (slot-value layer 'broadcast-address) (ip-interfaces:ip-interface-broadcast-address (interface layer))))

(defmethod make-frame-generator ((event Message) (layer UDP-Network-Layer))
  (let* ((json (trivial-json-codec:serialize-json event))
	 (frames (list (substitute #\_ #\- json :end (length json)))))
    (trivial-utilities:blambda ()
      (prog1
	  (car frames)
	(setf frames (cdr frames))))))

(defun decode-frame (data length)
  (declare (type integer length))
  (return-from decode-frame
    (the (values (or null message) &optional)
	 (make-message-from-frame (trivial-json-codec:deserialize-raw
			    (substitute #\- #\_ (babel:octets-to-string data :end length)))))))

(defmethod send ((message Message) (layer UDP-Network-Layer))
  (declare (type function on-complete-cb))

  (let ((resend-counter (redundant-messages-resend-count layer))
	(generated-message (funcall (or (make-frame-generator message layer)
					#'(lambda () (identity nil))))))

    (unless generated-message
      (log:warn "Message generation returned empty.")
      (return-from send))

    (labels ((send-this-frame (data)
	       (declare (type (simple-array (unsigned-byte 8) *) data))

	       (let ((sent-length (usocket:socket-send (socket layer) data (length data) :host (broadcast-address layer) :port (port layer))))
		 (unless (eq (length data) sent-length)
		   (log:error "Incorrect number of data sent (~a != ~a)."
			      (length data) sent-length)))
	       (return-from send-this-frame))
	     
	     (resend-this-frame (timer-id data)
	       (send-this-frame data)
	       (decf resend-counter)
	       (when (<= resend-counter 0)
		 (trivial-timer:cancel-timer-call timer-id))
	       
	       (return-from resend-this-frame)))

      (let ((data (babel:string-to-octets generated-message)))
	(send-this-frame data)
	(funcall (callback-fn message) message :sent)

	(when (redundant-messages-p layer)
	  (let ((timer-id 0))
	    (setf timer-id (trivial-timer:register-timer-recurring-call (redundant-messages-resend-interval layer) #'(lambda () (resend-this-frame timer-id data))))))))))

(defun receive-message (data length remote-host remote-port layer)
  (declare (ignore remote-host remote-port))
  (declare (type (simple-array (unsigned-byte 8) *) data)
	   (type fixnum length)
	   (type Network-Layer layer))

  (trivial-utilities:awhen (decode-frame data length)
    (funcall (on-receive-fn layer) it layer)))


(defmethod start ((layer UDP-Network-Layer))
  (setf (slot-value layer 'socket)
	(usocket:socket-connect nil nil :local-host (broadcast-address layer) :local-port (port layer) :protocol :datagram :timeout 0.001))
  (setf (usocket:socket-option (socket layer) :broadcast) t)

  ;; Unforturately usocket:socket-receive blocks even though :timeout was defined. So we cannot use trivial-monitored-thread.
  (setf (slot-value layer 'udp-receive-thread)
	(bordeaux-threads:make-thread
	 #'(lambda ()
	     (log:debug "UDP Network Layer '~a' started." (layer-name layer))
	     (iterate:iterate
	       (multiple-value-bind (data length remote-host remove-port)
		   (usocket:socket-receive (socket layer) (buffer layer) max-message-buffer-size)
		 (unless (equalp remote-host (ip-interfaces:ip-interface-address (interface layer)))
		   (handler-case
		       (receive-message data
					length
					(if (integerp remote-host)
					    (usocket:integer-to-octet-buffer remote-host (make-array 4) 4)
					    remote-host)
					remove-port
					layer)
		     (condition (c)
		       (break)
		       (log:error c))))
		 (iterate:until (shutdown-requested layer))
		 (sleep 0.001f0)))
	     (log:debug "UDP Network Layer '~a' ended." (layer-name layer)))
	 :name (concatenate 'string "UDP-Network-Layer--" (layer-name layer))))

  
  ;; @REVIEW This must all be given!
  ;;(trivial-monitored-thread:start-thread-monitor)
  ;;(start-response-monitor)
  ;;(trivial-timer:initialize-timer
  )

(defmethod stop ((layer UDP-Network-Layer))
  (setf (slot-value layer 'shutdown-requested) t)

  (when (udp-receive-thread layer)
    ;; Unforturately usocket:socket-receive blocks even though :timeout was defined.
    ;; So we have to kill the thread, instead of joining it...
    (bordeaux-threads:destroy-thread (udp-receive-thread layer))
    ;;(bordeaux-threads:join-thread (udp-receive-thread layer))
    (setf (slot-value layer 'udp-receive-thread) nil))
  
  (when (socket layer)
    (usocket:socket-close (socket layer))
    (setf (slot-value layer 'socket) nil))

  (setf (slot-value layer 'interface) nil)
  (setf (slot-value layer 'broadcast-address) nil)

  (return-from stop t))

