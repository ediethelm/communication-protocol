;;;; Copyright (c) Eric Diethelm 2021 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)

(defclass Parameter-Information-Service-Request (Service-Request)
  ((name :initform :PIS
	 :reader service-name
	 :type symbol
	 :allocation :class)
   (message-id :initform 966
	       :reader message-id
	       :type (unsigned-byte 10)
	       :allocation :class)
   (priority :initform :high
	     :reader service-priority
	     :type (member :high :low)
	     :allocation :class)
   (multi-frame :initform nil
		:reader multi-frame-p
		:type boolean
		:allocation :class)
   (parameter :initarg :parameter
	      :type string
	      :reader parameter)
   (read-data :initarg :read-data
	      :type integer
	      :reader read-data)
   (data :initarg :data
	 :type t
	 :reader data
	 :initform nil)))

(defclass Parameter-Information-Service-Response (Service-Response)
  ((name :initform :PIS
	 :reader service-name
	 :type symbol
	 :allocation :class)
   (message-id :initform 967
	       :reader message-id
	       :type (unsigned-byte 10)
	       :allocation :class)
   (priority :initform :high
	     :reader service-priority
	     :type (member :high :low)
	     :allocation :class)
   (multi-frame :initform nil
		:reader multi-frame-p
		:type boolean
		:allocation :class)
   (data :initarg :data
	 :reader data
	 :type t)))

(eval-when (:load-toplevel)
  (add-sre :pis-error 1 :parameter-not-found))

(defmethod populate-slots-from-frame ((message Parameter-Information-Service-Response) frame)
  (setf (slot-value message 'data) (get-value :data frame))
  (setf (slot-value message 'error-code) (get-sre-meaning (get-value :status frame))))
