;;;; Copyright (c) Eric Diethelm 2021 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)

(defclass Device-Maintenance-Service-Request (Service-Request)
  ((name :initform :DMS
	 :reader service-name
	 :type symbol
	 :allocation :class)
   (message-id :initform 960
	       :reader message-id
	       :type (unsigned-byte 10)
	       :allocation :class)
   (priority :initform :high
	     :reader service-priority
	     :type (member :high :low)
	     :allocation :class)
   (multi-frame :initform t
		:reader multi-frame-p
		:type boolean
		:allocation :class)
   (command :initarg :command
	    :reader dms-command
	    :type (member :reboot :maintenance))
   (argument :initarg :arguments
	     :initform 0
	     :reader dms-argument
	     :type (unsigned-byte 24))))

(defclass Device-Maintenance-Service-Response (Service-Response)
  ((name :initform :DMS
	 :reader service-name
	 :type symbol
	 :allocation :class)
   (message-id :initform 961
	       :reader message-id
	       :type (unsigned-byte 10)
	       :allocation :class)
   (priority :initform :high
	     :reader service-priority
	     :type (member :high :low)
	     :allocation :class)
   (multi-frame :initform nil
		:reader multi-frame-p
		:type boolean
		:allocation :class)))

(eval-when (:load-toplevel)
  (add-sre :dms-error 1 :file-not-found-error))

(defmethod populate-slots-from-frame ((message Device-Maintenance-Service-Response) frame)
  (setf (slot-value message 'error-code) (get-sre-meaning (get-value :ack frame))))
