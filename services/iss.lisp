;;;; Copyright (c) Eric Diethelm 2021 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)

(defclass Information-Setting-Service-Request (Service-Request)
  ((name :initform :PIS
	 :reader service-name
	 :type symbol
	 :allocation :class)
   (message-id :initform 964
	       :reader message-id
	       :type (unsigned-byte 10)
	       :allocation :class)
   (destination-sn :initarg :destination-sn
                   :reader destination-sn
                   :type string)
   (priority :initform :high
	     :reader service-priority
	     :type (member :high :low)
	     :allocation :class)
   (multi-frame :initform nil
		:reader multi-frame-p
		:type boolean
		:allocation :class)
   (new-id :initarg :new-id
	   :type (integer 1 511)
	   :reader new-id)))

(defclass Information-Setting-Service-Response (Service-Response)
  ((name :initform :PIS
	 :reader service-name
	 :type symbol
	 :allocation :class)
   (message-id :initform 965
	       :reader message-id
	       :type (unsigned-byte 10)
	       :allocation :class)
   (priority :initform :high
	     :reader service-priority
	     :type (member :high :low)
	     :allocation :class)
   (multi-frame :initform nil
		:reader multi-frame-p
		:type boolean
		:allocation :class)))

(eval-when (:load-toplevel)
  (add-sre :iss-error 1 :device-id-unchanged))

(defmethod populate-slots-from-frame ((message Information-Setting-Service-Response) frame)
  (setf (slot-value message 'error-code) (get-sre-meaning (get-value :status frame))))

