;;;; Copyright (c) Eric Diethelm 2021 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)

(defclass Data-Upload-Service-Request (Service-Request)
  ((name :initform :DUS
	 :reader service-name
	 :type symbol
	 :allocation :class)
   (message-id :initform 962
	       :reader message-id
	       :type (unsigned-byte 10)
	       :allocation :class)
   (priority :initform :high
	     :reader service-priority
	     :type (member :high :low)
	     :allocation :class)
   (multi-frame :initform t
		:reader multi-frame-p
		:type boolean
		:allocation :class)
   (md5 :initarg :md5
	:reader data-md5
        :type string)
   (data-length :initarg :data-length
		:reader data-length
                :type (unsigned-byte 24))
   (data :initarg :data
	 :type (or function
		   (simple-array (unsigned-byte 8) *))
	 :reader data)
   (current-frame :initform :start-frame
		  :reader current-frame
		  :type (member :start-frame :data-frame :end-frame))
   (data-frame-no :initform 0
		  :reader data-frame-no
		  :type (integer 0 *))))

(defclass Data-Upload-Service-Response (Service-Response)
  ((name :initform :DUS
	 :reader service-name
	 :type symbol
	 :allocation :class)
   (message-id :initform 963
	       :reader message-id
	       :type (unsigned-byte 10)
	       :allocation :class)
   (priority :initform :high
	     :reader service-priority
	     :type (member :high :low)
	     :allocation :class)
   (multi-frame :initform t
		:reader multi-frame-p
		:type boolean
		:allocation :class)))

(defmethod initialize-instance :after ((obj Data-Upload-Service-Request) &key)
  (unless (and (slot-boundp obj 'data)
	       (or (typep (data obj) 'simple-array)
		   (typep (data obj) 'function)))
    (error "The slot 'data' must always be given and be of a valid type!"))
  
  (when (and (typep (data obj) 'simple-array)
	     (equal (array-element-type (data obj))
		    '(unsigned-byte 8)))
    (when (and (slot-boundp obj 'data)
	       (not (slot-boundp obj 'data-length)))
      (setf (slot-value obj 'data-length) (length (data obj))))
    
    (when (and (slot-boundp obj 'data)
	       (not (slot-boundp obj 'md5)))
      (setf (slot-value obj 'md5) (ironclad:byte-array-to-hex-string
                                   (ironclad:digest-sequence :md5 (data obj))))))
  
  (when (typep (data obj) 'function)
    (unless (and (slot-boundp obj 'md5)
		 (not (null (data-md5 obj))))
      (error "MD5 must be given when 'data' slot is a function."))

    (unless (and (slot-boundp obj 'data-length)
		 (typep (data-length obj) '(integer 0 *)))
      (error "The length of the data must be given when 'data' slot is a function."))))


(defmethod populate-slots-from-frame ((message Data-Upload-Service-Request) frame)
  )

(defclass DUS-Start-Frame ()
  ((message-id :initarg :message-id
	       :type (unsigned-byte 10))
   (source-id :initarg :source-id
	      :type (unsigned-byte 9))
   (destination-id :initarg :destination-id
		   :type (unsigned-byte 9))
   (md5 :initarg :md5
        :type string)
   (data-size :initarg :data-size
              :type (unsigned-byte 24))))

(defclass DUS-Data-Frame ()
  ((message-id :initarg :message-id
	       :type (unsigned-byte 10))
   (source-id :initarg :source-id
	      :type (unsigned-byte 9))
   (destination-id :initarg :destination-id
		   :type (unsigned-byte 9))
   (data-crc :initarg :crc
	     :type string)
   (data :initarg :data
	 :type string)
   (frame-no :initarg :frame-no
	     :type (unsigned-byte 4))
   (expect-more :initarg :expect-more
		:type boolean)))

(defun calculate-frame-data (frame-no data-length size-limit)
  (declare (type (unsigned-byte 16) size-limit frame-no)
	   (type (unsigned-byte 24) data-length))
  (let* ((data-start (min data-length (* frame-no size-limit)))
	 (data-end (1- (min data-length (+ data-start size-limit))))
	 (too-large-for-one-frame (> (- data-length data-start) +SEND-LENGTH-LIMIT+)))
    (return-from calculate-frame-data (list data-start data-end too-large-for-one-frame))))

(defmethod make-frame-generator ((service Data-Upload-Service-Request) (layer UDP-Network-Layer))
  (trivial-utilities:blambda ()
    (let ((srv service)) ;; Capture the service object
      (case (current-frame srv)
	(:start-frame
	 (return (substitute #\_ #\-
			     (trivial-json-codec:serialize-json
			      (make-instance 'DUS-Start-Frame
					     :message-id (message-id srv)
					     :source-id (source-id srv)
					     :destination-id (destination-id srv)
					     :md5 (data-md5 srv)
                                             :data-size (data-length srv))))))
	(:data-frame
	 (destructuring-bind (data-start data-end too-large-for-one-frame)
	     (calculate-frame-data (data-frame-no srv) (data-length srv) +SEND-LENGTH-LIMIT+)

	   (log:trace "Creating data frame no. ~a" (data-frame-no srv))
	   (when (> data-end data-start)
	     (let ((data (subseq (data srv) data-start (1+ data-end))))
	       (return (substitute #\_ #\-
				   (trivial-json-codec:serialize-json
				    (make-instance 'DUS-Data-Frame
						   :message-id (message-id srv)
						   :source-id (source-id srv)
						   :destination-id (destination-id srv)
						   :frame-no (data-frame-no srv)
						   :data (ironclad:byte-array-to-hex-string data)
						   :crc (ironclad:byte-array-to-hex-string
							 (ironclad:digest-sequence :crc32 data))
						   :expect-more (if too-large-for-one-frame 1 0)))))))
	   (log:error "No data to be sent in frame no. ~a" (data-frame-no srv))
	   (return nil)))
	(:end-frame
	 (return nil))))))


(eval-when (:load-toplevel)
  (add-sre :dus-error 1 :tmp-file-error)
  (add-sre :dus-error 2 :crc-error)
  (add-sre :dus-error 3 :frame-scrambled-error)
  (add-sre :dus-error 4 :update-package-oversize-error)
  (add-sre :dus-error 5 :md5-error)
  (add-sre :dus-error 6 :update-finalization-error)
  (add-sre :dus-error 7 :empty-data-error))

(defmethod make-response-handler ((service Data-Upload-Service-Request))
  (let ((srv service)) ;; Capture the service object
    (return-from make-response-handler
      (trivial-utilities:blambda (message)
	(let ()
	  (declare (type Data-Upload-Service-Response message))

          (funcall (callback-fn srv) srv :response-received :response message)
          (unless (eq :no-error (error-code message))
            (funcall (callback-fn service) service :service-request-failed :reason (error-code message))
          (return nil))

          (case (current-frame srv)
            (:start-frame
	     ;; @TODO What happens whe the device is not ready to receive?
	     ;;(when (eq (ready-to-receive message) 1)
	     (setf (slot-value srv 'current-frame) :data-frame)
	     (setf (slot-value srv 'data-frame-no) 0)

	     ;; Remember to reset the message counter so the router does not consider it a redundant message
	     (setf (slot-value srv 'counter) nil)
	     (return srv))
	    (:data-frame
	     ;; @TODO Resend previous frame when a CRC error is reported, and also consider other error-codes

	     (destructuring-bind (data-start data-end too-large-for-one-frame)
		 (calculate-frame-data (1+ (data-frame-no srv)) (data-length srv) +SEND-LENGTH-LIMIT+)
	       (declare (ignore too-large-for-one-frame))
	       
	       (if (> data-end data-start)
		   (progn
		     (incf (slot-value srv 'data-frame-no))

		     ;; Remember to reset the message counter so the router does not consider it a redundant message
		     (setf (slot-value srv 'counter) nil)
		     (return srv))
		   (progn
		     (setf (slot-value srv 'current-frame) :end-frame)
		     (funcall (callback-fn srv) srv :service-request-successful)
		     (return nil)))))
	    (:end-frame
	     (return-from nil))))))))

(defmethod populate-slots-from-frame ((message Data-Upload-Service-Response) frame)
  (setf (slot-value message 'error-code) (get-sre-meaning (get-value :error-code frame))))

