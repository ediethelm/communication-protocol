;;;; Copyright (c) Eric Diethelm 2021 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)


(defclass Communication-Interface ()
  ((network-router :reader network-router
		   :type Network-Router
		   :initarg :network-router)
   (client-layer :reader client-layer
		 :type (or null Client-Layer)
		 :initarg client-layer
		 :initform nil)))

(defun default-callback (message code &rest args)
  (declare (ignore args))
  (log:info "Default callback from id ~a with code ~a" (source-id message) code))

(defun default-receive-fn (message layer)
  (log:info "Default receive message ~a from device ~a via layer ~a"
	    (message-id message) (source-id message) (layer-name layer)))

(defun default-status-change-fn (device-id status)
  (log:info "Default status change handler: device ~a has now status ~a"
	    device-id status))

(defun default-service-request-fn ()
  (error "Not implemented yet!"))

(defun default-failure-fn (dev-id msg-id)
  (declare (ignore dev-id msg-id))
  (error "Not implemented yet!"))

(defun add-known-device (dev-id layer-name ci)
  (let ((layer (find-layer-from-name layer-name (network-layers (network-router ci)))))
    (unless layer
      (log:error "The layer named '~a' does not exist." layer-name)
      (return-from add-known-device nil))
    (add-device dev-id layer ci)))

(defun add-device (dev-id layer ci)
  (push (make-instance 'device-tracker :device-id dev-id :last-activity-ts (get-universal-time)
				       :dhe-offset 60 :next-hb-expected-ts (+ 120 (get-universal-time))
				       :origin-layer layer :device-status :unknown)
	(slot-value (network-router ci) 'known-devices))
  (return-from add-device t))

(defun remove-known-device (dev-id layer-name ci)
  (let ((layer (find-layer-from-name layer-name (network-layers (network-router ci)))))
    (unless layer
      (log:error "The layer named '~a' does not exist." layer-name)
      (return-from remove-known-device nil))
    (remove-device dev-id layer ci)))

(defun remove-device (dev-id layer ci)
    (setf (slot-value (network-router ci) 'known-devices)
	  (delete-if #'(lambda (x) (and (eq (device-id x)  dev-id)
					(eq (origin-layer x) layer)))
		     (slot-value (network-router ci) 'known-devices)))
  (return-from remove-device t))

(defun send-message (message communication-interface)
  (forward-message message (client-layer communication-interface)))

(defun make-communication-interface (config &key on-event-fn on-service-request-fn on-device-status-changed-fn)
  (declare (type function on-event-fn on-service-request-fn))
  (let* ((ci (make-instance 'Communication-Interface))
 	 (routing-table (getf config :routing-table))
 	 (fsm (getf config :device-state-fsm))
	 (client-layer-found nil))
    (flet ((on-receive (message layer) (receive message layer (network-router ci))))
      ;; Create network router
      (setf (slot-value ci 'network-router)
	    (make-instance 'Network-Router
			   :routing-table routing-table
			   :network-layers (iterate:iterate
					     (iterate:for layer-config in (getf config :network-layers))
					     (iterate:collecting (make-network-layer layer-config #'on-receive ci)))
			   :on-device-status-changed-callback on-device-status-changed-fn
			   :fsm (make-instance 'Action-FSM :descriptor fsm)))
      
      ;; Verify one and only one Client-Layer exists
      ;; and set callback functions
      (iterate:iterate
	(iterate:for layer in (network-layers (network-router ci)))
	(when (eq :client (layer-type layer))
	  (when client-layer-found
	    (error "Only one :client layer can be defined for one communication interface."))

	  (setf client-layer-found t)
	  (setf (slot-value layer 'on-event-callback) on-event-fn)
	  (setf (slot-value layer 'on-service-request-callback) on-service-request-fn)
	  (setf (slot-value ci 'client-layer) layer)))

      (unless client-layer-found
	(error "No client layer defined!"))
      
      (return-from make-communication-interface ci))))


(defmethod start ((ci Communication-Interface))
  (update-message-id-to-class-map)
  (start (network-router ci)))

(defmethod stop ((ci Communication-Interface))
  (stop (network-router ci)))
