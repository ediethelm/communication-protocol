;;;; Copyright (c) Eric Diethelm 2022 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)

(defclass Action-FSM ()
  ((descriptor :type list
	       :reader descriptor
	       :initform nil
	       :initarg :descriptor)))

(defun make-action-fsm (descriptor)
  (make-instance 'Action-FSM :descriptor descriptor))

(defgeneric act-on/state (state action state-holder fsm))
(defmethod act-on/state ((state t) (action t) (state-holder t) (fsm t))
  (declare (ignore state-holder fsm))
  (log:error "No transition for: ~a -> ~a" state action)
  (return-from act-on/state state))

(defun act-on (state-holder fsm action)
  (let ((allowed-states (getf (descriptor fsm) (device-status state-holder)))
	(next-state (act-on/state (device-status state-holder) action state-holder fsm)))
    (unless (member next-state allowed-states)
      (log:error "Invalid transition from '~a' to '~a' requested!" (device-status state-holder) next-state)
      (setf next-state (device-status state-holder)))

    (return-from act-on next-state)))

(defclass Device-Tracker ()
  ((device-id :reader device-id
	      :type (integer 1 *)
	      :initarg :device-id)
   (serial-number :initarg :serial-number
		  :reader serial-number
		  :type (unsigned-byte 32))
   (next-hb-expected-ts :reader next-hb-expected-ts
			:type (unsigned-byte 64)
			:initarg :next-hb-expected-ts)
   (last-activity-ts :reader last-activity-ts
		     :type (unsigned-byte 64)
		     :initarg :last-activity-ts)
   (dhe-offset :reader dhe-offset
	       :type (unsigned-byte 16)
	       :initarg :dhe-offset )
   (origin-layer :reader origin-layer
		 :type Network-Layer
		 :initarg :origin-layer)
   (device-status :reader device-status
		  :type (member :probably-operational :operational :maintenance :recovery :uncommissioned :offline :power-up :online :crippled :locked :blacklisted)
		  :initarg :device-status)))


(defun set-last-activity-ts (ts tracker)
  (setf (slot-value tracker 'last-activity-ts) ts))

(defmethod act-on/state ((state (eql :unknown)) (message Event) tracker fsm)
  (set-last-activity-ts (get-universal-time) tracker)
  (return-from act-on/state :probably-operational))

(defmethod act-on/state (state (message Event) tracker fsm)
  (set-last-activity-ts (get-universal-time) tracker)
  (return-from act-on/state state))

(defmethod act-on/state ((state (eql :maintenance)) (message Event) tracker fsm)
  (set-last-activity-ts (get-universal-time) tracker)
  (return-from act-on/state :blacklisted))

(defmethod act-on/state ((state (eql :locked)) (message Event) tracker fsm)
  (set-last-activity-ts (get-universal-time) tracker)
  (return-from act-on/state :blacklisted))

(defmethod act-on/state ((state (eql :blacklisted)) (message Event) tracker fsm)
  (set-last-activity-ts (get-universal-time) tracker)
  (return-from act-on/state :blacklisted))

(defmethod act-on/state (state (action (eql :timer)) tracker fsm)
  (when (member state '(:unknown :blacklisted))
    (return-from act-on/state state))

  (let ((now (get-universal-time)))
    (when (and (< (next-hb-expected-ts tracker) now)
	       (< (+ (last-activity-ts tracker) (dhe-offset tracker)) now))
      (log:warn "The Device ~a did not respond in time (now: ~a expected: ~a)"
		(device-id tracker) now (next-hb-expected-ts tracker))
      (return-from act-on/state :offline))

    (return-from act-on/state state)))
