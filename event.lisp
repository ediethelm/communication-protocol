;;;; Copyright (c) Eric Diethelm 2021 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)


(defclass Event (Message)
  ((rtr :initarg :rtr
	:type boolean
	:initform nil
	:accessor rtr)))


(defmethod populate-slots-from-frame :before ((message Event) frame)
  (setf (slot-value message 'rtr) (get-value :rtr frame)))
