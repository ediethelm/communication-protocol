;;;; Copyright (c) Eric Diethelm 2021 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)

(defparameter *message-id-to-class-map* nil)


(defclass Message ()
  ((message-id :reader message-id
	       :type (unsigned-byte 10)
	       :allocation :class)
   (source-id :initarg :source-id
	      :type (unsigned-byte 9)
	      :accessor source-id)
   (counter :initarg :counter
	    :type (unsigned-byte 8)
	    :reader message-counter)
   (callback :initarg :callback-fn
	     :reader callback-fn
	     :type function
	     :initform #'default-callback)
   (timestamp :initarg :timestamp
	      :type (unsigned-byte 64)
	      :accessor timestamp)))

(defgeneric slots-excluded-from-serialization (message)
   (:method-combination append))

(defmethod slots-excluded-from-serialization append ((message Message))
  '(callback timestamp))

(defmethod make-frame-generator :around ((message Message) layer)
  (let ((trivial-json-codec:*slots-to-ignore-in-serialization* (slots-excluded-from-serialization message)))
    (call-next-method)))

(defun get-value (key obj)
  (cadr (assoc key obj)))

(defun update-message-id-to-class-map ()
  (labels ((collect-subclasses (c)
	     (loop for sc in (closer-mop:class-direct-subclasses c)
		   appending (cons sc (collect-subclasses sc)))))
    (setf *message-id-to-class-map*
	  (iterate:iterate (iterate:for c in (collect-subclasses (find-class 'Message)))
			   (trivial-utilities:awhen (find-if #'(lambda (slotd)
								 (and (eq (closer-mop:slot-definition-allocation slotd) :class)
								      (eq (closer-mop:slot-definition-name slotd) 'message-id)
								      (not (null (closer-mop:slot-definition-initform slotd)))))
							     (closer-mop:class-direct-slots c))
			     
			     (iterate:collect (cons (closer-mop:slot-definition-initform it) c)))))))

(defun make-message-from-frame (frame)
  "Creates a new Message instance of the correct subclass."
  (flet ((find-class-for-message-id (message-id)
	   (cdr (assoc message-id *message-id-to-class-map*))))
    (let* ((message-class (find-class-for-message-id (cadr (assoc :message-id frame))))
	   (message nil))
      (if message-class
	  (progn
	    (setf message (make-instance message-class))
	    (populate-slots-from-frame message frame))
	  (log:error "No class found to process the message id ~a" (cadr (assoc :message-id frame))))
      (return-from make-message-from-frame (the (or null Message) message)))))

(defgeneric populate-slots-from-frame (message frame))

(defmethod populate-slots-from-frame ((message t) frame)
  (log:warn "No explicit constructor for message ID ~a defined." (message-id message)))

(defmethod populate-slots-from-frame :before ((message Message) frame)
  (setf (slot-value message 'source-id) (get-value :source-id frame))
  (setf (slot-value message 'counter) (get-value :counter frame))
  (setf (slot-value message 'timestamp)
	(trivial-utilities:aif (get-value :timestamp frame)
			       it
			       (get-universal-time))))

