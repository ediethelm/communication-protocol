;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :communication-protocol
  :name "Communication Protocol for Marvin devices"
  :description "Marvin: The buttler."
  :long-description ""
  :version "0.9.0"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  ;;:in-order-to ((test-op (test-op :communication-protocol/test)))
  :depends-on (:trivial-utilities
	       :trivial-monitored-thread
	       :trivial-object-lock
	       :trivial-json-codec
	       :trivial-timer
	       :piggyback-parameters
	       :ironclad
	       :log4cl
	       :arnesi
	       :usocket
	       :ip-interfaces
	       :hunchensocket)
  :serial t
  :components ((:file "package")
	       (:file "message")
	       (:file "event")
	       (:file "service-request")
	       (:file "service-response")
	       (:file "device-operating-mode")
	       (:file "network-layer")
	       (:file "network-router")
	       (:file "communication-interface")
	       (:module "network-layers"
		:depends-on ("network-layer")
		:components ((:file "udp-network-layer")
			     (:file "websocket-network-layer")
			     (:file "client-layer")
			     ))
	       (:module "services"
		:depends-on ("service-request" "service-response")
		:components ((:file "dms")
			     (:file "dus")
			     (:file "iss")
			     (:file "pis")))
	       (:module "events"
		:depends-on ("event")
		:components ((:file "event-208")
			     (:file "event-209")
			     (:file "event-252"))))
  :in-order-to ((test-op (test-op :communication-protocol/test))))


(defsystem :communication-protocol/test
  :name "communication-protocol/test"
  :description "Unit Tests for the communication-protocol project."
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:communication-protocol
	       :websocket-driver-client
	       :fiveam)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :communication-protocol-tests))
  :components ((:file "test-communication-protocol")))
