;;;; Copyright (c) Eric Diethelm 2021 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)

(defclass Service-Request (Message)
  ((name :reader service-name
	 :type symbol
	 :allocation :class)
   (priority :reader service-priority
	     :type (member :high :low)
	     :allocation :class)
   (destination-id :initarg :destination-id
		   :reader destination-id
		   :initform 0
		   :type (unsigned-byte 9))
   (multi-frame :reader multi-frame-p
		:type boolean
		:allocation :class)
   (frame-generator :reader frame-generator-fn
		    :type function)
   (response-handler-generator :reader response-handler-generator-fn
			       :type function)
   (current-frame :initform :none
		  :reader current-frame
		  :type (member :none))
   (frame-resend-counter :initform 10
			 :accessor frame-resend-counter
			 :type (unsigned-byte 8))))

(defmethod initialize-instance :after ((instance Service-Request) &key)
  (setf (slot-value instance 'response-handler-generator) (make-response-handler instance)))


(defmethod slots-excluded-from-serialization append ((message Service-Request))
  '(name priority multi-frame frame-generator response-handler-generator current-frame frame-resend-counter))

(defmethod populate-slots-from-frame :before ((message Service-Request) frame)
  )
