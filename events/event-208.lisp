(in-package :communication-protocol)

(defclass Event-208 (Event)
  ((message-id :type (unsigned-byte 10)
	       :reader message-id
	       :initform 208
	       :allocation :class)
   (offset :initarg :offset
	   :type (unsigned-byte 16)
	   :reader offset)
   (operating-mode :initarg :operating-mode
	   :type (unsigned-byte 8)
	   :reader operating-mode)))


(defmethod populate-slots-from-frame ((message Event-208) frame)
  (setf (slot-value message 'offset) (get-value :offset frame))
  (setf (slot-value message 'operating-mode) (get-value :status frame)))

(defun set-next-expected-hb (offset tracker tolerance)
  (let* ((now (get-universal-time))
	 (ts (the (unsigned-byte 62) (+ now (* offset tolerance)))))
    (declare (type (unsigned-byte 62) ts))
    (setf (slot-value tracker 'next-hb-expected-ts) ts)))

(defmethod act-on/state (state (message Event-208) tracker fsm)
  (set-next-expected-hb (offset message) tracker 3)
  (status-from-message-value (operating-mode message)))

(defmethod act-on/state ((state (eql :blacklisted)) (message Event-208) tracker fsm)
  (set-next-expected-hb (offset message) tracker 3)
  (return-from act-on/state :blacklisted))
