(in-package :communication-protocol)

(defclass Event-252 (Event)
  ((message-id :type (unsigned-byte 10)
	       :reader message-id
	       :initform 252
	       :allocation :class)
   (serial-number :reader serial-number
		  :type (unsigned-byte 32)
		  :initarg :serial-number)
   (operating-mode :reader operating-mode
		  :type (unsigned-byte 8)
		  :initarg :operating-mode)))


(defmethod populate-slots-from-frame ((message Event-252) frame)
  (setf (slot-value message 'serial-number) (get-value :serial-number frame))
  (setf (slot-value message 'operating-mode) (get-value :operating-mode frame)))

(defmethod act-on/state (state (message Event-252) tracker fsm)
  (set-last-activity-ts (get-universal-time) tracker)
  (status-from-message-value (operating-mode message)))


