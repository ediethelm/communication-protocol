(in-package :communication-protocol)

(defclass Event-209 (Event)
  ((message-id :type (unsigned-byte 10)
	       :reader message-id
	       :initform 209
	       :allocation :class)
   (network-time :initarg :network-time
	   :type (unsigned-byte 64)
	   :reader network-time)))


(defmethod populate-slots-from-frame ((message Event-209) frame)
  (setf (slot-value message 'network-time) (get-value :network-time frame)))
