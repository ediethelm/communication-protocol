(uiop:define-package #:communication-protocol
  (:documentation "")
  (:use #:common-lisp)
  (:export #:Communication-Interface
	   #:make-communication-interface
	   #:send-message
	   #:start
	   #:stop
	   #:add-known-device
	   #:remove-known-device
	   #:default-callback
	   #:default-receive-fn
	   #:default-status-change-fn
	   #:default-service-request-fn
	   #:counter
	   #:message
	   #:message-id
	   #:source-id
	   #:message-counter
	   #:event
	   #:ttl
	   #:offset
	   #:serial-number
	   #:service-name
	   #:service-priority
	   #:multi-frame-p
	   #:frame-resend-counter
	   #:populate-slots-from-frame
	   #:Device-Maintenance-Service-Request
	   #:Information-Setting-Service-Request
	   #:Parameter-Information-Service-Request
	   #:Data-Upload-Service-Request))


(in-package :communication-protocol)

(defgeneric start (obj))
(defgeneric stop (obj))
