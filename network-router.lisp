;;;; Copyright (c) Eric Diethelm 2021 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)

(defclass Network-Router ()
  ((routing-table :reader routing-table
		  :type list
		  :initarg :routing-table
		  :initform nil)
   (network-layers :reader network-layers
		   :type list ;;(proper-list Network-Layer)
		   :initarg :network-layers
		   :initform nil)
   (known-devices :accessor known-devices
		  :type list ;; (proper-list device-status-tracker)
		  :initform nil)
   (message-counter :initform 0
		    :type (unsigned-byte 8))
   (cleanup-thread :type t
		   :reader cleanup-thread
		   :initform nil)
   (redundant-messages :type list
		       :accessor redundant-messages
		       :initform nil)
   (on-device-status-changed-callback :reader on-device-status-changed-callback
				      :type function
				      :initarg :on-device-status-changed-callback)
   (heartbeat-monitoring-thread-id :initform nil
				   :type (or null (integer 0 *))
				   :reader heartbeat-monitoring-thread-id)
   (fsm :initarg :fsm
	:initform (error "Define a FSM for possible device status transitions.")
	:accessor fsm
	:type Action-FSM)
   (offset-tolerance :initarg :offset-tolerance
		     :initform 3
		     :accessor offset-tolerance
		     :type (integer 1 *)
		     :documentation "We give every Device a tolerance of 3 times its offset to send it's heartbeat message")))

(defmethod start ((router Network-Router))
  (setf (slot-value router 'cleanup-thread)
	(trivial-timer:register-timer-recurring-call 1000 #'(lambda () (cleanup-redundant-messages router))))

  (setf (slot-value router 'heartbeat-monitoring-thread-id)
	(trivial-monitored-thread:make-monitored-thread (:name "Device-Heartbeat-Monitoring-Thread" :sleep-duration 5 :max-restarts 5)
	  (update-device-heartbeat-status router)))

  (loop for layer in (network-layers router) do
    (start layer)))

(defmethod stop ((router Network-Router))
  (loop for layer in (network-layers router) do
    (stop layer))

  (trivial-monitored-thread:stop-thread (heartbeat-monitoring-thread-id router))
  (trivial-monitored-thread:join-thread (heartbeat-monitoring-thread-id router))

  (setf (slot-value router 'heartbeat-monitoring-thread-id) nil)

  (trivial-timer:cancel-timer-call (cleanup-thread router))
  (setf (slot-value router 'cleanup-thread) nil)
  (setf (slot-value router 'redundant-messages) nil))

(defun match-message-id (message-id routing-table-entry)
  (let ((entry-message-id (first routing-table-entry)))
    (etypecase entry-message-id
      (symbol (eq :any entry-message-id))
      (cons (<= (first entry-message-id) message-id (second entry-message-id)))
      ((integer 0 *) (eq message-id entry-message-id)))))

(defun match-device-id (device-id routing-table-entry)
  (when (eq 0 device-id)             ;; Handle broadcast messages, such as RTR 252 or ISS
    (return-from match-device-id t))

  (let ((entry-device-id (first routing-table-entry)))
    (etypecase entry-device-id
      (symbol (eq :any entry-device-id))
      (cons (<= (first entry-device-id) device-id (second entry-device-id)))
      ((integer 0 *) (eq device-id entry-device-id)))))

(defun match-source-layer (layer routing-table-entry)
  (let ((entry-source-layer (third routing-table-entry)))
    (cond ((eq :any entry-source-layer)
	   t)
	  ((eq :none entry-source-layer)
	   nil)
	  ((typep entry-source-layer 'string)
	   (if (string-equal entry-source-layer (layer-name layer))
	       t
	       nil))
	  (t (eq entry-source-layer (layer-type layer))))))

(defun collect-layers-to-reroute (msg-id dev-id layer router)
  (let ((routing-table (routing-table router)))
    (iterate:iterate
      (iterate:for entry in routing-table)
      (when
	  (and (match-message-id msg-id entry)
	       (match-device-id dev-id entry)
	       (match-source-layer layer entry))
	(iterate:until (eq :none (cadddr entry)))
	(iterate:collect (cadddr entry))))))

(defun get-tracker (dev-id layer router)
  (find-if #'(lambda (x) (and (eq (device-id x)  dev-id)
			      (eq (origin-layer x) layer)))
	   (known-devices router)))

(defun find-layers-for-device (dev-id router)
  (if (eq 0 dev-id)
      (network-layers router)
      (mapcar #'origin-layer (remove-if-not #'(lambda (x) (eq x dev-id)) (known-devices router) :key #'device-id))))

(defun find-layer-from-name (layer-name network-layers)
  (find layer-name network-layers :test #'string-equal :key #'layer-name))

(defun find-layers-of-type (layer-type network-layers)
  (remove-if-not #'(lambda (x) (eq layer-type (layer-type x))) network-layers))

(defun get-next-msg-counter (router)
  (if (eq 255 (slot-value router 'message-counter))
      (setf (slot-value router 'message-counter) 0)
      (incf (slot-value router 'message-counter))))

(defun push-message (message router)
  (let ((pair (list (cons (get-universal-time) message))))
    (if (redundant-messages router)
	(nconc (redundant-messages router) pair)
	(setf (redundant-messages router) pair))))

(defun translate-source-id (message)
  (if (typep message 'Service-Request)
      (destination-id message)
      (source-id message)))

(defun redundant-message-p (message router)
  (let ((dev-id (translate-source-id message))
	(msg-id (message-id message))
	(msg-cnt (message-counter message)))
    (not (null (remove-if-not #'(lambda (elm)
				  (and (eq dev-id (translate-source-id (cdr elm)))
				       (eq msg-id (message-id (cdr elm)))
				       (eq msg-cnt (message-counter (cdr elm)))))
			      (redundant-messages router))))))

(defun cleanup-redundant-messages (router)
  (let ((now (get-universal-time)))
    (setf (redundant-messages router)
	  (delete-if #'(lambda (elm)
			 (< (car elm) (- now 5)))
		     (redundant-messages router)))))

(defun receive (message layer router)
  (let ((destination-id (translate-source-id message)))
    (if (or (not (slot-boundp message 'counter))
	    (null (slot-value message 'counter)))
	(setf (slot-value message 'counter) (get-next-msg-counter router))
	(let ((tracker (get-tracker (source-id message) layer router)))
	  (unless (or tracker
		      (eq (layer-type layer) :client))
	    (setf tracker (make-instance 'device-tracker :device-id (source-id message) :last-activity-ts (get-universal-time)
							 :dhe-offset 60 :next-hb-expected-ts (+ 120 (get-universal-time))
							 :origin-layer layer :device-status :unknown))
	    (push tracker (known-devices router)))

	  ;; Update device status and known layers
	  (when tracker
	    (update-device-status message tracker router))

	  (when (redundant-message-p message router)
	    (log:trace "Discarded redundant message '~a' from '~a'." (message-id message) destination-id)
	    (return-from receive))))

    (push-message message router)

    (iterate:iterate
      (iterate:for destination-layer in
		   (remove-duplicates
		    (iterate:iterate
		      (iterate:with network-layers = (network-layers router))
		      (iterate:for layer-to-send in (collect-layers-to-reroute (message-id message) destination-id layer router))
		      (cond ((eq :any layer-to-send)
			      (iterate:appending network-layers))
			    ((eq :known-to-layer layer-to-send)
			     (iterate:appending (find-layers-for-device destination-id router)))
			    ((typep layer-to-send 'string)
			     (iterate:collect (find-layer-from-name layer-to-send network-layers)))
			    (t
			     (iterate:appending (find-layers-of-type layer-to-send network-layers)))))))

      (unless (or (null destination-layer)
	          (eq destination-layer layer))
	(send message destination-layer)))))


(defun status-from-message-value (value)
  (case value
    (0 :power-up)
    (6 :offline)
    (7 :online)
    (8 :uncommissioned)
    (9 :recovery)
    (10 :maintenance)
    (20 :operational)
    (90 :locked)))

(defun update-device-status (message  tracker router)
  (let* ((state (device-status tracker))
	 (next-state (act-on tracker (fsm router) message)))
    ;; @TODO Should devices be considered separately when they exist on multiple layers or as one device? (as they really are)

    (unless (eq next-state state)
      (setf (slot-value tracker 'device-status) next-state)
      (funcall (on-device-status-changed-callback router) (source-id message) next-state))))

(defun update-device-heartbeat-status (router)
  (let ((fsm (fsm router)))
    (trivial-object-lock:with-object-lock-held ((known-devices router))
      (loop
	for tracker in (known-devices router)
	do
	   (act-on tracker fsm :timer)))))

#|
 Retransmission Routing
 The retransmission routing table consists of entries in the form
 (<msg-id> <dev-id> <origin-layer> <routing-layer>)

Fields
msg-id (integer |
        <Range (from [integer] to [integer])> |
        :any)
dev-id (integer |
        <Range (from [integer] to [integer])> |
        :any)
origin-layer (layer instance |
              layer type |
              :any)
destination-layer (layer instance |
                   layer type |
                   :any |
                   :none |
                   :known-to-layer)

 Special entries are:
 (:any :any :any :any) -> Reroute all
 (:any :any :any :none) -> Deny all

 Example
 ((:any :any :any #client-layer)  -> all messages from other layers are forwarded to the client layer
  (:any :any #client-layer :known-to-layer)) -> any message from the client layer is forwarded to the layer which knows the device id



 |#
