;;;; Copyright (c) Eric Diethelm 2022 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)

(5am:def-suite :communication-protocol-tests :description "Communication Protocol tests")
(5am:in-suite :communication-protocol-tests)
(setf 5am:*on-error* :debug)

(defparameter config/routing-table '((:any :any :any "Client Layer")
				     (:any :any :any "Debug Layer")
				     (:any :any "Client Layer" :known-to-layer)))

(defparameter config/network-layers '((:type :client :name "Client Layer" :source-id 1)
				      (:type :udp :name "UDP Layer" :port 4242)
				      (:type :web-socket :name "WebSocket Layer" :port 8081)))

(defparameter config/device-state-fsm '(:unknown (:unknown :operational :maintenance :uncommissioned :recovery :locked :crippled :blacklisted :probably-operational :offline)
					:probably-operational (:operational :maintenance :blacklisted :probably-operational :offline)
					:operational (:operational :maintenance :probably-operational :crippled :locked :blacklisted :offline)
					:maintenance (:operational :maintenance :probably-operational :locked :blacklisted :offline)
					:recovery (:operational :maintenance :probably-operational :recovery :offline :blacklisted)
					:uncommissioned (:operational :maintenance :probably-operational :uncommissioned :offline :blacklisted)
					:offline (:operational :maintenance :probably-operational :recovery :offline :blacklisted)
					:crippled (:operational :maintenance :probably-operational :recovery :crippled :offline :blacklisted)
					:locked (:operational :maintenance :recovery :locked :offline :blacklisted)
					:blacklisted (:blacklisted)))


(defparameter received-message nil)

(defclass Debug-Layer (Network-Layer)
  ((type :initform :debug
	 :type symbol
	 :reader layer-type
	 :allocation :class)))

(defmethod start ((layer Debug-Layer)))
(defmethod stop ((layer Debug-Layer)))

(defmethod send ((message Message) (layer Debug-Layer))
  (setf received-message message))

(defun simulate-receive (message layer)
  (funcall (on-receive-fn layer) message layer))

(defun on-receive (message layer)
  (declare (ignore layer))
  (setf received-message message))

(defun make-ci ()
  "Helpful for manual, exploratory testing."
  (setf hunchentoot:*catch-errors-p* nil)
  (trivial-timer:initialize-timer)
  (trivial-monitored-thread:start-thread-monitor)

  (let ((config `(:routing-table ,config/routing-table
		  :network-layers config/network-layers
		  :device-state-fsm ,config/device-state-fsm))
	(ci nil))

    (setf ci (make-communication-interface config
					   :on-event-fn #'(lambda (message layer)
							    (log:info "Just received message ~a from device ~a via layer ~a"
								      (message-id message) (source-id message) (layer-name layer)))
					   :on-service-request-fn #'default-service-request-fn
					   :on-device-status-changed-fn #'default-status-change-fn))
    (start ci)
    (return-from make-ci ci)))

(defun initialize (communication-interface)
  ;;(setf hunchentoot:*catch-errors-p* nil)
  (trivial-timer:initialize-timer)
  (trivial-monitored-thread:start-thread-monitor)
  (start communication-interface))

(defun shutdown (communication-interface)
  (stop communication-interface)
  (trivial-monitored-thread:stop-thread-monitor)
  (trivial-timer:shutdown-timer)
  (sleep 0.1))

(defparameter config `(:routing-table ,config/routing-table
		       :network-layers ,(append config/network-layers '((:type :debug :name "Debug Layer")))
		       :device-state-fsm ,config/device-state-fsm))

(5am:test make-communication-interface
  (setf received-message nil)
  (let ((ci (make-communication-interface config :on-event-fn #'on-receive :on-service-request-fn #'default-service-request-fn :on-device-status-changed-fn #'default-status-change-fn)))

    (unwind-protect
	 (progn
	   (5am:finishes (initialize ci))
	   (let ((msg1 (make-instance 'event-208 :source-id 7 :counter 1 :offset 60))
		 (msg2 (make-instance 'event-209 :source-id 3 :counter 2 :network-time 123))
		 (msg3 (make-instance 'event-252 :source-id 9 :counter 3 :operating-mode 1 :serial-number 10203040))
		 (dbg-layer (find "Debug Layer" (network-layers (network-router ci)) :key #'layer-name :test #'string-equal)))
	     (5am:is-true dbg-layer)

	     ;; Routing table entry (:any :any "Client Layer" :known-to-layer)
	     (5am:finishes (add-known-device 7 "Debug Layer" ci))
	     (5am:finishes (send-message msg1 ci))
	     (5am:is (eq msg1 received-message))
	     
	     (5am:is (eq (car (find-layers-for-device 7 (network-router ci)))
			 dbg-layer))

	     (setf received-message nil)
	     
	     ;; Routing table entry (:any :any :any "Debug Layer")
	     (5am:finishes (send-message msg2 ci))
	     (5am:is (eq msg2 received-message))

	     (setf received-message nil)
	     
	     ;; Routing table entry (:any :any :any "Default Client Layer")
	     (5am:finishes (simulate-receive msg3 dbg-layer))
	     (5am:is (eq msg3 received-message))))
      (5am:finishes (shutdown ci)))))
    
    ;;(collect-layers-to-reroute (make-instance 'event-208 :source-id 7) (car (network-layers (network-router com-int))) (network-router com-int))
    ;;(find-layers-of-type :client (network-layers (network-router com-int)))
    ;;(make-network-layer '(:type :client :name "Client Layer") #'identity)))



(5am:test websocket-layer
  (let ((ci (make-communication-interface config
					  :on-event-fn #'(lambda (message layer)
							   (break "Receiving a message...")
							   (log:info "Just received message ~a from device ~a via layer ~a"
								     (message-id message) (source-id message) (layer-name layer)))
					  :on-service-request-fn #'default-service-request-fn
					  :on-device-status-changed-fn #'default-status-change-fn))
	(client (wsd:make-client "ws://localhost:8081/ws-network" :additional-headers '(("User-Agent" . "Marvin Device"))))
	(ws-layer nil)
	(message nil))
    (unwind-protect
	 (progn
	   (5am:finishes (initialize ci))
	   
	   (setf ws-layer (find-layer-from-name "WebSocket Layer" (network-layers (network-router ci))))
	   (5am:is (typep ws-layer 'WebSocket-Network-Layer))
	   (5am:finishes (add-known-device 3 "WebSocket Layer" ci))
	   
	   (wsd:start-connection client)
	   (wsd:on :message client
		   (lambda (msg)
		     (setf message
			   (make-message-from-frame (trivial-json-codec:deserialize-raw
				     (substitute #\- #\_ msg))))))

	   ;; check no subscriptions
	   (5am:is (eq (subscriptions ws-layer) nil))
	   
	   (wsd:send client "{\"subscription-action\" : \"subscribe\", \"source_id\" : 3, \"message_id\" : 209}")
	   (sleep 2)

	   ;; check for this subscription
	   (5am:is-true (member-if #'(lambda (x)
				       (eq (getf x :message-id) 209))
				   (second (assoc 3 (subscriptions ws-layer)))))

	   ;; send a message via client-layer
	   (send-message (make-instance 'event-209 :source-id 3 :counter 4 :network-time 235) ci)
	   (sleep 2)
	   
	   ;; check message received over ws
	   (5am:is-true (and message
			     (eq (source-id message) 3)
			     (eq (message-id message) 209)
			     (eq (network-time message) 235)
			     (eq (message-counter message) 4)))
	   (setf message nil)
	   
	   (wsd:send client "{\"subscription-action\" : \"unsubscribe\", \"source_id\" : 3, \"message_id\" : 209}")
	   (sleep 2)
	   
	   ;; check no subscriptions
	   (5am:is (eq (subscriptions ws-layer) nil))
	   
	   ;; send another message -> check no errors
	   (send-message (make-instance 'event-209 :source-id 3 :counter 5 :network-time 235) ci)
	   (5am:is (null message)))
      
      (wsd:close-connection client)
      (sleep 3)
      (5am:finishes (shutdown ci))
      )))



(5am:test service-routing
  (setf received-message nil)
  (let ((ci (make-communication-interface config :on-event-fn #'on-receive :on-service-request-fn #'default-service-request-fn :on-device-status-changed-fn #'default-status-change-fn)))

    (unwind-protect
	 (progn
	   (5am:finishes (initialize ci))
	   (let ((srv1 (make-instance 'Device-Maintenance-Service-Request :command :reboot :destination-id 99 :counter 0 :source-id 1))
		 (srv2 (make-instance 'Device-Maintenance-Service-Response :source-id 99 :error-code 0 :counter 2))
		 (dbg-layer (find "Debug Layer" (network-layers (network-router ci)) :key #'layer-name :test #'string-equal)))
	     (5am:is-true dbg-layer)

	     ;; Routing table entry (:any :any "Client Layer" :known-to-layer)
	     (5am:finishes (add-known-device 99 "Debug Layer" ci))
	     (5am:finishes (send-message srv1 ci))
	     (5am:is (eq srv1 received-message))
	     
	     (setf received-message nil)

	     ;; Routing table entry (:any :any :any "Client Layer")
	     (5am:finishes (simulate-receive srv2 dbg-layer))
	     (5am:is (eq nil received-message))))
      (5am:finishes (shutdown ci)))))
