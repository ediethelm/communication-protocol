;;;; Copyright (c) Eric Diethelm 2021 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)


(defclass Service-Response (Message)
  ((multi-frame :reader multi-frame-p
		:type boolean
		:allocation :class)
   (error-code :initarg :error-code
	       :reader error-code
	       :type keyword)))

(defvar service-response-errors
  '((0 . :no-error)
    (1 . :invalid-message-id)
    (2 . :invalid-message-type)
    (3 . :service-handler-occupied)
    (4 . :invalid-operation)
    (9 . :internal-state-error)
    (10 . :dis-error)
    (20 . :dds-error)
    (30 . :dus-error)
    (40 . :iss-error)
    (50 . :pis-error)
    (60 . :dms-error)
    (127 . :unknown-error)))


(defun get-sre-code (meaning)
  (trivial-utilities:aprog1
      (car (rassoc meaning service-response-errors))
    (unless it
      (log:error "The requested error description '~a' is not known" meaning)
      (return-from get-sre-code 127))))

(defun get-sre-meaning (code)
  (trivial-utilities:aprog1
      (cdr (assoc code service-response-errors))
    (unless it
      (log:error "The requested error code '~a' is not known" code)
      (return-from get-sre-meaning :unknown-error))))

(defun add-sre (category code-delta meaning)
  (let ((base-code (car (rassoc category service-response-errors))))
    (unless base-code
      (log:error "The error category '~a' is not known." category)
      (return-from add-sre nil))

    (let ((sre-code (+ base-code code-delta)))
      (when (rassoc meaning service-response-errors)
	(log:error "The error description '~a' already exists and is represented by code '~a'. The error code '~a' will now also represent this."
		   meaning (car (rassoc meaning service-response-errors)) sre-code))

      (trivial-utilities:aif (assoc sre-code service-response-errors)
			     (progn
			       (log:error "The error code '~a' already exists and represents '~a'. It will be now replaced by '~a'."
					  sre-code (cdr (assoc sre-code service-response-errors)) meaning)
			       (setf (cdr it) meaning))
			     (push (cons sre-code meaning) service-response-errors)))))


(defmethod slots-excluded-from-serialization append ((message Service-Response))
  '(multi-frame))

(defmethod populate-slots-from-frame :before ((message Service-Response) frame)
  )

(defmethod make-response-handler ((service Service-Request))
  #'(lambda (message)
      (declare (type Service-Response message))
      (log:trace "Received '~a' response for ~a request from ~a." (error-code message) (service-name message) (source-id message))
      (funcall (callback-fn service) service :response-received :response message)

      (if (eq :no-error (error-code message))
        (funcall (callback-fn service) service :service-request-successful)
        (funcall (callback-fn service) service :service-request-failed :reason (error-code message)))))

