;;;; Copyright (c) Eric Diethelm 2021 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :communication-protocol)

(defparameter *network-layer-sub-class-map* nil)

(defclass Network-Layer ()
  ((type :type symbol
	 :reader layer-type
	 :allocation :class)
   (name :type string
	 :reader layer-name
	 :initarg :name)
   (ci :type Communication-Interface
       :reader communication-interface
       :initarg :ci)
   (on-receive-fn :reader on-receive-fn
		  :type function
		  :initarg :on-receive-fn)))



(defgeneric send (message layer))

(defgeneric load-configuration (layer config))
(defmethod load-configuration ((layer Network-Layer) config))

(defun update-network-layer-sub-class-map ()
  (labels ((collect-subclasses (c)
	     (loop for sc in (closer-mop:class-direct-subclasses c)
		   appending (cons sc (collect-subclasses sc)))))
    (setf *network-layer-sub-class-map*
	  (iterate:iterate (iterate:for c in (collect-subclasses (find-class 'Network-Layer)))
			   (trivial-utilities:awhen (find-if #'(lambda (slotd)
								 (and (eq (closer-mop:slot-definition-allocation slotd) :class)
								      (eq (closer-mop:slot-definition-name slotd) 'type)
								      (not (null (closer-mop:slot-definition-initform slotd)))))
							     (closer-mop:class-direct-slots c))
			     
			     (iterate:collect (cons (closer-mop:slot-definition-initform it) c)))))))


(defun make-network-layer (config on-receive ci)
  "Creates a new Network-Layer instance of the correct subclass."

  (unless *network-layer-sub-class-map*
    (update-network-layer-sub-class-map))
  
  (flet ((find-class-for-layer-type (layer-type)
	   (cdr (assoc layer-type *network-layer-sub-class-map*))))
    (let* ((layer-class (find-class-for-layer-type (getf config :type)))
	   (layer (make-instance layer-class
				 :name (getf config :name)
				 :ci ci
				 :on-receive-fn on-receive)))
      (load-configuration layer config)
      (return-from make-network-layer (the Network-Layer layer)))))


